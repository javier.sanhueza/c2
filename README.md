## Instrucciones de instalación

```
npm install
```

### Instalación de Typescript

```
npm install -g typescript
```

### Instalación de jest

```
npm install -g jest
```

## Levantando el proyecto

```
npm start
```

## Ejecutándo los tests

```
npm run test
```

# Happy coding!!!
